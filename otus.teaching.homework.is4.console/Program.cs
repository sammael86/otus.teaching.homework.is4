﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;

namespace otus.teaching.homework.is4.console
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var client = new HttpClient();

            var clientTokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = "https://localhost:5011/connect/token",
                ClientId = "console_app",
                ClientSecret = "secret",
                Scope = "api1"
            });

            Console.WriteLine("Client token response:");
            Console.WriteLine(clientTokenResponse.Raw);
            Console.WriteLine();
            
            client.SetBearerToken(clientTokenResponse.AccessToken);

            var apiResponse = await client.GetStringAsync("https://localhost:5001/WeatherForecast");

            Console.WriteLine("Api response:");
            Console.WriteLine(apiResponse);
        }
    }
}