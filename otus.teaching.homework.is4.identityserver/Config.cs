﻿using IdentityServer4.Models;
using System.Collections.Generic;

namespace otus.teaching.homework.is4.identityserver
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new("api1", "Full access to Weather forecast API"),
            };

        public static IEnumerable<ApiResource> ApiResources =>
            new ApiResource[]
            {
                new("api1", "Weather forecast API") { Scopes = { "api1" } }
            };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                new()
                {
                    ClientId = "weather_forecast_swagger",
                    ClientName = "Swagger UI for weather forecast",
                    ClientSecrets = { new Secret("secret".Sha256()) },

                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    RequireClientSecret = false,

                    RedirectUris = { "https://localhost:5001/swagger/oauth2-redirect.html" },
                    AllowedCorsOrigins = { "https://localhost:5001" },
                    AllowedScopes = { "api1" }
                },
                new()
                {
                    ClientId = "console_app",
                    ClientName = "Console App for IS4 test",
                    ClientSecrets = { new Secret("secret".Sha256()) },

                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    AllowedScopes = { "api1" }
                }
            };
    }
}